import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<User> userList = new ArrayList<>();
        // subTask3

        User user1 = new User("123456", "Tam Nguyen", true);
        User user2 = new User("123456", "Minh Khai", false);
        User user3 = new User();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        for (User user : userList) {
            System.out.println(user);
        }
    }
}
