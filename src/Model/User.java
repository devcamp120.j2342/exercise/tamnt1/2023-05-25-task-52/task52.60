// subTask1
public class User {
    private String paswsword;
    private String username;
    private boolean enabled;

    // subTask2
    public User() {
    }

    public User(String paswsword, String username, boolean enabled) {
        this.paswsword = paswsword;
        this.username = username;
        this.enabled = enabled;
    }

    public String getPaswsword() {
        return paswsword;
    }

    public void setPaswsword(String paswsword) {
        this.paswsword = paswsword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    // subTask3
    @Override
    public String toString() {
        return "User [username=" + username + ", enabled=" + enabled + "]";
    }
}